package de.tarent.maven.plugins;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import org.evolvis.FRSPackage;
import org.evolvis.FRSRelease;
import org.evolvis.GForgeAPIPortTypeProxy;
import org.evolvis.User;

public class GForgeLib {
	
//	/** The api port. */
//	private GForgeAPIPortType apiPort;
	
	/** The ApiPortTypeProxy*/
	private GForgeAPIPortTypeProxy proxy;
	
	/** The Log String*/
	private String log;
	
	/** The ErrorLog String*/
	private String errorLog;
	
	
	public GForgeLib(String gForgeAPIPort_address){
		this.log = "";
		this.errorLog = "";
		GForgeAPIPortTypeProxy proxy = new GForgeAPIPortTypeProxy();
		proxy.setEndpoint(gForgeAPIPort_address);
		this.setProxy(proxy);
	}
	
	/**
	 * Encode a file with base64.
	 * 
	 * @param fileURI the file uri of the file to encode
	 * 
	 * @return the string of the encoded file
	 */
	public static String encodeBase64(String fileURI) {
		File file = new File(fileURI);
		DataInputStream in;
		byte[] buffer = null;
		try {
			in = new DataInputStream(new FileInputStream(file));
			buffer = new byte[(int) file.length()];
			in.readFully(buffer);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Base64.encode(buffer);
	}
	
	/**
	 * Gets the GForge user id.
	 * 
	 * @param gforgeSessionID the gforge session id
	 * @return the GForge user id
	 */
	public int getGForgeUserID(String gforgeSessionID, String userName){
		User[] userInfo = null;
		try {
			userInfo = proxy.getUsersByName(gforgeSessionID,new String[] { userName });
			//print some info
			addLog("GForge user: " + userName + " (user ID: "
					+ String.valueOf(userInfo[0].getUser_id()) + ")");
			addLog("real name: "+userInfo[0].getFirstname() + " "
					+ userInfo[0].getLastname());
		} catch (RemoteException e) {
			e.printStackTrace();
			this.addExceptionToLog(e);
		}
		//return user ID
		return userInfo[0].getUser_id();
	}
	
	/**
	 * Gets the file typeID.
	 * 
	 * @param fileName the file name
	 * 
	 * @return the file type
	 */
	public static int getFileType(String fileName) {
		StringTokenizer st = new StringTokenizer(fileName,".");
		String ending = "";
		while (st.hasMoreTokens()) {
			ending = st.nextToken();
		}
		boolean source = false;
		if (fileName.contains("source"))
			source = true; 
		if (ending.trim().equalsIgnoreCase("deb"))
			return 1000;
		else if (ending.trim().equalsIgnoreCase("rpm")){
			if (source) {
				return 5100;
			}else{
				return 2000;
			}
		}else if (ending.trim().equalsIgnoreCase("zip")){
			if (source) {
				return 5000;
			}else{
				return 3000;
			}
		}else if (ending.trim().equalsIgnoreCase("bz2")){
			if (source) {
				return 5010;
			}else{
				return 3100;
			}
		}else if (ending.trim().equalsIgnoreCase("gz")){
			if (source) {
				return 5020;
			}else{
				return 3110;
			}
		}else if (ending.trim().equalsIgnoreCase("jpg"))
			return 8000;
		else if (ending.trim().equalsIgnoreCase("text"))
			return 8100;
		else if (ending.trim().equalsIgnoreCase("html"))
			return 8200;
		else if (ending.trim().equalsIgnoreCase("pdf"))
			return 8300;
		else
			if (source)
				return 5900;
			else
				return 9999;
	}
	
	/**
	 * Gets the processor id.
	 * 
	 * @param processorType the processor type
	 * 
	 * @return the processor id
	 */
	public static int getProcessorID(String processorType) {
		if (processorType.trim().equalsIgnoreCase("i386"))
			return 1000;
		if (processorType.trim().equalsIgnoreCase("IA64"))
			return 6000;
		if (processorType.trim().equalsIgnoreCase("Alpha"))
			return 7000;
		if (processorType.trim().equalsIgnoreCase("Any"))
			return 8000;
		if (processorType.trim().equalsIgnoreCase("PPC"))
			return 2000;
		if (processorType.trim().equalsIgnoreCase("MIPS"))
			return 3000;
		if (processorType.trim().equalsIgnoreCase("Sparc"))
			return 4000;
		if (processorType.trim().equalsIgnoreCase("UltraSparc"))
			return 5000;
		if (processorType.trim().equalsIgnoreCase("Other"))
			return 9999;
		if (processorType.trim().equalsIgnoreCase("ARM"))
			return 9000;
		else
			return 9999;
	}
	

	/**
	 * Gets the GForge group id to a <i>projectName</i>.
	 * 
	 * @param gforgeSessionID the gforge session id
	 * @param projectName the project name
	 * @param gforgeUserID the gforge user id
	 * 
	 * @return the group id
	 */
	public int getGForgeProjectID(String gforgeSessionID, String projectName, int gforgeUserID){
		int gforgeGroupID = -1;
		// try to find the GForge groupID 
		try {
			search: {
				// GForge groups are projects
				org.evolvis.Group[] usersGroups = proxy.userGetGroups(
						gforgeSessionID, gforgeUserID);
				for (int i = 0; i < usersGroups.length; i++) {
					// search for group with maven artifact name
					if (usersGroups[i].getGroup_name().trim().equalsIgnoreCase(projectName)) {
						gforgeGroupID = usersGroups[i].getGroup_id();
						break search;
					}
				}
				// throw exception if no package matches the name
				this.addErrorLog("no GForge project(group) found with name '" + projectName
						+ "' or you don't have permissions");
			}// end search
		} catch (RemoteException e) {
			e.printStackTrace();
			this.addExceptionToLog(e);
		}
		return gforgeGroupID;
	}
	
	/**
	 * Searchs if there is only one package available in this group and returns the ID of this package. 
	 * 
	 * @param gforgeSessionID the gforge session id
	 * @param gforgeGroupID the gforge group id
	 * 
	 * @return the package ID.  
	 */
	public int getGForgePackageID(String gforgeSessionID, int gforgeGroupID){
		return getGForgePackageID(gforgeSessionID, gforgeGroupID, null);
	}
	
	/**
	 * Gets the package id.
	 * 
	 * @param gforgeSessionID the gforge session id
	 * @param gforgeGroupID the gforge group id
	 * @param packageName the name of the GForge package
	 * 
	 * @return the package ID of the package with <i>packageName</i>. If <i>packageName</i> 
	 * is null and only one package is available, the ID of this package is returned. 
	 */
	public int getGForgePackageID(String gforgeSessionID, int gforgeGroupID, String packageName){
		int gforgePackageID = -1;
		try {
			FRSPackage[] packages = proxy.getPackages(gforgeSessionID,gforgeGroupID);
			//check if package name is set (in pom)
			package_search:
			if (packageName != null){
				for (int i=0;i<packages.length;i++){
					if (packages[i].getName().trim().equals(packageName.trim())){
						gforgePackageID = packages[i].getPackage_id();
						break package_search;
					}
				}
			}else{
				if (packages.length == 1) {
					gforgePackageID = packages[0].getPackage_id();
				} else {
					throw new RuntimeException("packageName is 'null' and GForge project has more than one package, specify name!");
				}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
			this.addExceptionToLog(e);
		}
		return gforgePackageID;
	}
	
	/**
	 * Creates a GForge package 
	 * 
	 * @param gforgeSessionID the gforge session id
	 * @param gforgeGroupID the gforge group id
	 * @param packageName the name of the GForge package
	 * 
	 * @return the package ID of the package.
	 */
	public int createGForgePackage(String gforgeSessionID, int gforgeGroupID, String packageName){
		int gforgePackageID = -1;
		addLog("Creating package with name '"+packageName+"'!");
		try {
			gforgePackageID = proxy.addPackage(gforgeSessionID,
					gforgeGroupID, packageName, 1);
		} catch (RemoteException e) {
			e.printStackTrace();
			this.addExceptionToLog(e);
		}
		return gforgePackageID;
	}

	
	/**
	 * Gets the package name.
	 * 
	 * @param gforgeSessionID the gforge session id
	 * @param gforgeGroupID the gforge group id
	 * @param gforgePackageID the ID of a GForge package
	 * 
	 * @return the package name of the package with <i>gforgePackageID</i>. 
	 */
	public String getGForgePackageName(String gforgeSessionID, int gforgeGroupID, int gforgePackageID){
		String gforgePackageName = "";
		try {
			FRSPackage[] packages = proxy.getPackages(gforgeSessionID,gforgeGroupID);
			//check if package name exists
			package_search:{
				for (int i=0;i<packages.length;i++){
					if (packages[i].getPackage_id()==gforgePackageID){
						gforgePackageName = packages[i].getName();
						break package_search;
					}
				}
				this.addErrorLog("package with id '"+gforgePackageID+"' not found!");
			}	
		} catch (RemoteException e) {
			e.printStackTrace();
			this.addExceptionToLog(e);
		}
		return gforgePackageName;
	}

	/**
	 * Creates the release (with empty release notes and changes) if release with this name does not exist.
	 * 
	 * @param gforgeSessionID the gforge session id
	 * @param gforgeGroupID the gforge group id
	 * @param gforgePackageID the gforge package id
	 * @param releaseName the release number
	 * 
	 * @return the release ID
	 */
	public int createGForgeRelease(String gforgeSessionID, int gforgeGroupID, int gforgePackageID, String releaseName){
		return  createGForgeRelease(gforgeSessionID, gforgeGroupID, gforgePackageID, releaseName, "","");
	}
			
	/**
	 * Creates the release if release with this name does not exist.
	 * 
	 * @param gforgeSessionID the gforge session id
	 * @param gforgeGroupID the gforge group id
	 * @param gforgePackageID the gforge package id
	 * @param releaseName the release number
	 * @param releaseNotes the release notes
	 * @param releaseChanges the release changes
	 * 
	 * @return the release ID
	 */
	public int createGForgeRelease(String gforgeSessionID, int gforgeGroupID, int gforgePackageID, String releaseName, String releaseNotes, 
			String releaseChanges){
		int releaseID = -1;
		try {
			FRSRelease[] releases = proxy.getReleases(gforgeSessionID,gforgeGroupID,gforgePackageID);
			boolean create = true;
			searchReleases:{
				//check if release already exists
				for (int i = 0; i < releases.length; i++) {
					if (releases[i].getName().trim().equalsIgnoreCase(releaseName)){
						releaseID = releases[i].getRelease_id();
						//getLog().warn("release "+releaseNumber+" already exists! Doing nothing!");
						create=false;
						break searchReleases;
					}
				}
			}
			if (create){
				//create the release
				addLog("* creating new release " + releaseName);
				releaseID = proxy.addRelease(gforgeSessionID, gforgeGroupID,
						gforgePackageID, releaseName, releaseNotes, releaseChanges,
						0);
			}
		} catch (RemoteException e) {
			e.printStackTrace();
			this.addExceptionToLog(e);
		}
		return releaseID;
	}
	

	/**
	 * Upload all files with the given file types to release.
	 * 
	 * @param fileTypes the file types
	 * @param folder location of the files to upload
	 * @param releaseName of the files to upload
	 * @param gforgePackageName the gforge package name of the release
	 * @param gforgeProjectID the gforge project id of the release
	 */
	public void uploadFilesToGForge(String gforgeSessionID, List<String> fileTypes, String folder, String releaseName, String gforgePackageName, int gforgeProjectID){	
		uploadFilesToGForge(gforgeSessionID, fileTypes, null, folder, releaseName, gforgePackageName, gforgeProjectID, "", "", "other");
	}

	/**
     * Upload all files with the given file types to release.
     * 
     * @param fileTypes the file types
     * @param folder location of the files to upload
     * @param releaseName of the files to upload
     * @param gforgePackageName the gforge package name of the release
     * @param gforgeProjectID the gforge project id of the release
     * @param releaseNotes release Notes 
     * @param releaseChanges release changes
     * @param processorType processor type
     */
    public void uploadFilesToGForge(String gforgeSessionID, List<String> fileTypes,
            String folder, String releaseName, String gforgePackageName, 
            int gforgeProjectID, String releaseNotes, String releaseChanges, String processorType){
		uploadFilesToGForge(gforgeSessionID, fileTypes, null, folder, releaseName, gforgePackageName, 
		        gforgeProjectID, releaseName, releaseChanges, processorType);
    }

	/**
	 * Upload all files with the given file types to release.
	 * 
	 * @param fileTypes the file types
	 * @param folder location of the files to upload
	 * @param filePrefix prefix filter for files to upload (set to null if none needed)
	 * @param releaseName of the files to upload
	 * @param gforgePackageName the gforge package name of the release
	 * @param gforgeProjectID the gforge project id of the release
	 * @param releaseNotes release Notes 
	 * @param releaseChanges release changes
	 * @param processorType processor type
	 */
	public void uploadFilesToGForge(String gforgeSessionID, List<String> fileTypes,
	        String filePrefix, String folder, String releaseName, String gforgePackageName, 
	        int gforgeProjectID, String releaseNotes, String releaseChanges, String processorType){
		Iterator<String> fileTypesIt = fileTypes.iterator();
		File dir = new File(folder);
		addLog("");
		addLog(gforgePackageName.toUpperCase());
		addLog("------------------------------------------------------------------------");
		//get all matching files in directory for each filetype
		while (fileTypesIt.hasNext()){
			String currentFileType = fileTypesIt.next();	
			//create new filter
			String filterString = "";
			if (filePrefix != null) {
			    filterString = filePrefix;
			    if (currentFileType.trim().equals("")){
			        currentFileType = "binary";
			    }
			} else if (currentFileType.trim().equals("")){
				//case for binary files
				filterString = gforgePackageName+ "-" + releaseName+ ".";
				currentFileType="binary";
			}else if(currentFileType.trim().equals("deb")){
				filterString = gforgePackageName+ "_" + releaseName + "_all.deb";
			}else{
				filterString = gforgePackageName+ "-" + releaseName +"-"+currentFileType + ".";
			}
			//setLog("Filterstring: "+filterString);
			final FilenameFilter filter = new FileTypeFilter(filterString);
			addLog("using prefix filter '" + filterString + "' to filter release files");
			//list files in directory (filtered by filter)
			final String[] files = dir.list(filter);
			if (files==null || files.length==0) {
				addLog(currentFileType.toUpperCase()+": no files found");
			} else {
				int gforgePackageID = getGForgePackageID(gforgeSessionID, gforgeProjectID,gforgePackageName);
				//creates a new package if package does not exist
				if (gforgePackageID==-1)
					gforgePackageID = createGForgePackage(gforgeSessionID, gforgeProjectID, gforgePackageName);
				//create a release
				int gforgeReleaseID = createGForgeRelease(gforgeSessionID, gforgeProjectID, gforgePackageID, releaseName, releaseNotes, releaseChanges);
				//for all files
				for (int i=0;i<files.length;i++){
					final File uploadFile = new File(dir.toString()+"/"+files[i]);
					if (!uploadFile.isFile()) {
					    addLog("skipping (not regular file) " + dir);
					    continue;
					}
                    addLog("* uploading file: "+files[i]+" ["+uploadFile.length()+" Bytes]");
					// load and encode File in base64
					String fileContent = encodeBase64(dir.toString()+"/"+files[i]);
					try {
						//upload the file
						proxy.addFile(gforgeSessionID, gforgeProjectID, gforgePackageID,
								gforgeReleaseID, files[i], fileContent, getFileType(files[i]), getProcessorID(processorType), 0);
					} catch (RemoteException e) {
						this.addExceptionToLog(e);
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	/**
	 * Creates the GForge session id.
	 * 
	 * @return the GForge session id
	 */
	public String createGForgeSession(String userName, String password) {
		String gForgeSessionID = "";
			try {
				//create session ID
				gForgeSessionID = proxy.login(userName,password);
			} catch (IOException e) {
				System.out.println("wrong username/password for GForge?");
				this.addExceptionToLog(e);
				e.printStackTrace();
			}
		return gForgeSessionID;
	}
	
	/**
	 * Closes a GForge session.
	 * 
	 * @param gForgeSessionID the session ID of the session
	 */
	public void closeGForgeSession(String gForgeSessionID){
		try {
			//close session
			this.proxy.logout(gForgeSessionID);
		} catch (IOException e) {
			this.addExceptionToLog(e);
			e.printStackTrace();
		}
	}
	
	public String getLog() {
		return log;
	}

	public void addLog(String log) {
		this.log = this.getLog()+"\n"+log;
	}
	
	public String getErrorLog() {
		return errorLog;
	}

	private void addErrorLog(String errorLog) {
		this.errorLog = this.getErrorLog()+"\n"+errorLog;
	}

	public GForgeAPIPortTypeProxy getProxy() {
		return proxy;
	}

	public void setProxy(GForgeAPIPortTypeProxy proxy) {
		this.proxy = proxy;
	}

	private void addExceptionToLog(Exception e){
		this.addErrorLog("\n#####"+e.getClass().getName()+"######################################");
		this.addErrorLog("StackTrace: ");
		this.addErrorLog(e.getMessage());
		StackTraceElement[] stackTrace = e.getStackTrace();
		for (int i=0;i<stackTrace.length;i++)
			this.addErrorLog(stackTrace[i].toString());
	}

}
