/*
 * 
 * maven-gforge-plugin,
 * Maven Plugin to to upload released files to a gforge server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'maven-gforge-plugin'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.maven.plugins;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.apache.maven.shared.release.config.PropertiesReleaseDescriptorStore;
import org.apache.maven.shared.release.config.ReleaseDescriptor;

/**
 * Goal <i>prepare</i> prepares the upload of the files.
 * 
 * @goal prepare
 */
public class PrepareGForge extends AbstractMojo {
	

	/**
	 * The maven Project
	 * 
	 * @parameter expression="${project}"
	 */
	private MavenProject project;

	/* (non-Javadoc)
	 * @see org.apache.maven.plugin.AbstractMojo#execute()
	 */
	public void execute() throws MojoExecutionException, MojoFailureException {
		if (this.project.isExecutionRoot()){
			File releasePropertiesFile = new File(project.getBasedir().toString()+"/release.properties");
			Map<String, String> releaseNumberMap = this.readReleaseNumbersFromFile(releasePropertiesFile);
			if (!releaseNumberMap.isEmpty())
				this.saveReleaseNumbersToXMLFile(releaseNumberMap, project.getBasedir().toString()+"/gforge.releasenumber");
		}else{
			getLog().info("Not master project -> omitting ..");
		}
	}

	/**
	 * Gets the release numbers from the 'release.properties' file.
	 * 
	 * @return a map with the release numbers (including all subprojects)
	 */
	public Map<String, String> readReleaseNumbersFromFile(File file){
		PropertiesReleaseDescriptorStore prds = new PropertiesReleaseDescriptorStore();
		ReleaseDescriptor rd = null;
		Map<String, String> versionMap = new HashMap<String, String>();
		String version = "";
		try {
			getLog().info("Using release property file: "+file.toString());
			rd = prds.read(new File(project.getBasedir().toString()+"/release.properties"));
			version =  rd.getReleaseVersions().toString();
			version = version.replace("{", "") ;
			version = version.replace(",", "") ;
			version = version.replace(project.getGroupId()+":", "§") ;
			version = version.replace("}", "") ;
		} catch (Exception e) {
			System.out.println("File 'release.properties' missing?");
			e.printStackTrace();
		}
		StringTokenizer st = new StringTokenizer(version,"§");
		while (st.hasMoreTokens()){
			String token = st.nextToken();
			StringTokenizer st2 = new StringTokenizer(token,"=");
			if (st2.hasMoreTokens()){
				String key = st2.nextToken();
				if (st2.hasMoreTokens()){
					String value = st2.nextToken();
					versionMap.put(key.trim(), value.trim());
				}
			}
		}
		getLog().info("found "+versionMap.size()+" release number(s)");
		return versionMap;
	}
	
	/**
	 * Save release number to a file.
	 * 
	 * @param releaseNumber the release number
	 * @param filename the filename
	 */
	public void saveReleaseNumbersToXMLFile(Map<String, String> releaseNumbers, String filename){
		 try {
			FileWriter out = new FileWriter (filename);
			out.write("<?xml version=\"1.0\"?>\n<releaseNumbers>");
			Set<String> keys = releaseNumbers.keySet();
			Iterator<String> it = keys.iterator();
			while (it.hasNext()){
				String currentKey = it.next();
				String value = releaseNumbers.get(currentKey);
				out.write("\n<"+currentKey+">"+value+"</"+currentKey+">");
			}
			out.write("\n</releaseNumbers>");
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
}
