/*
 * maven-gforge-plugin,
 * Maven Plugin to to upload released files to a gforge server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'maven-gforge-plugin'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.maven.plugins;

import java.io.File;
import java.io.FilenameFilter;

/**
 * The Class FileTypeFilter.
 */
public class FileTypeFilter implements FilenameFilter {

	/** The filename prefix for the filter. */
	private String beginning;
	
	/**
	 * Instantiates a new file type filter.
	 * 
	 * @param beginning the beginning
	 */
	public  FileTypeFilter(String beginning){
		this.beginning = beginning;
	}
	
    /* (non-Javadoc)
     * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
     */
    public boolean accept(File dir, String name) {
        return name.startsWith(this.beginning);
    }

}
