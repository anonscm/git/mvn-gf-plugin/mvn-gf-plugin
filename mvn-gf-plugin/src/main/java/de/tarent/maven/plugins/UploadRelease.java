/*
 * maven-gforge-plugin,
 * Maven Plugin to to upload released files to a gforge server
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'maven-gforge-plugin'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.maven.plugins;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

/**
 * Goal <i>upload</i> uploads the released files.
 * 
 * @goal upload
 * 
 */
public class UploadRelease extends AbstractMojo {

	/**
	 * The maven Project
	 * 
	 * @parameter expression="${project}"
	 */
	private MavenProject project;

	/**
	 * Release Notes
	 * 
	 * @parameter expression="${releaseNotes}" default-value=""
	 */
	private String releaseNotes;

	/**
	 * Release Changes
	 * 
	 * @parameter expression="${releaseChanges}" default-value=""
	 */
	private String releaseChanges;

	/**
	 * Release Number
	 * 
	 * @parameter expression="${releaseNumber}"
	 */
	private String releaseNumber;

	/**
	 * Path to the GForge user data file
	 * 
	 * @parameter expression="${user.home}/.m2"
	 */
	private String gForgeUserDataPath;

	/**
	 * Filename of the GForge user data file
	 * 
	 * @parameter default-value="gforge.access"
	 */
	private String gForgeUserDataFile;

	/**
	 * Name of the GForge Project
	 * 
	 * @parameter expression="${gForgeProject}"
	 */
	private String gForgeProject;

	/**
	 * Path to the files to upload
	 * 
	 * @parameter expression="${uploadFilePath}"
	 *            default-value="target/checkout/"
	 */
	private String uploadFilePath;

	/**
	 * Processor type of the released files
	 * 
	 * @parameter default-value="Any"
	 */
	private String processorType;

	/**
	 * gForgeAPIPort_address
	 * 
	 * @parameter required
	 */
	private String gForgeAPIPort_address;

	/**
	 * List of file types to upload.
	 * 
	 * @parameter
	 */
	private List<String> fileTypes;
	
	/**
     * Prefix to filter the files to upload.
     * If not set, <code>gforgePackageName + "-" + releaseName + "."</code> 
     * is used.
     * 
     * @parameter
     */
    private String filePrefix;

	private GForgeLib gForgeLib;

	private String gForgeSessionID;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.maven.plugin.AbstractMojo#execute()
	 */
	@SuppressWarnings("unchecked")
	public void execute() throws MojoExecutionException {
		// only execute plugin if master-project
		if (this.project.isExecutionRoot()) {
			if (this.gForgeAPIPort_address == null
					|| this.gForgeAPIPort_address.trim().equals(""))
				throw new RuntimeException(
						"gForgeAPIPort_address or gForgeNameSpace is empty! Defined in pom?");
			else {
				this.setGForgeLib(new GForgeLib(this.gForgeAPIPort_address));
			}
			getLog().info("mvn-gforge-plugin:upload");
			// create List with file types
			List<String> fileTypes = new ArrayList<String>();
			// add this for binary Files
			fileTypes.add("");
			// add other file types from pom
			if (this.fileTypes != null)
				fileTypes.addAll(this.fileTypes);

			// get GForge UserID, ProjectID, PackageID
			String projectName = "";
			if (this.gForgeProject != null) {
				projectName = this.gForgeProject;
			} else {
				projectName = project.getArtifactId();
			}
			File userDataFile = new File(this.gForgeUserDataPath + "/"
					+ this.gForgeUserDataFile);
			this.setGForgeSessionID(this.getGForgeLib().createGForgeSession(
					getGForgeConfigData("user", userDataFile),
					getGForgeConfigData("password", userDataFile)));
			int gforgeUserID = this.getGForgeLib().getGForgeUserID(
					this.getGForgeSessionID(),
					getGForgeConfigData("user", userDataFile));
			int gforgeProjectID = this.getGForgeLib().getGForgeProjectID(
					this.getGForgeSessionID(), projectName, gforgeUserID);

			// get list of all modules(for each module a project name will be
			// created)
			List<String> packageNames = new ArrayList<String>();
			// add this for the masterpackage (same name as project)
			packageNames.add("");
			// add all sub-packages
			packageNames.addAll(project.getModules());
			Iterator<String> it = packageNames.iterator();
			while (it.hasNext()) {
				String gforgePackageName = it.next();
				// get the package name
				String packageName = "";
				if (gforgePackageName.trim().equals("")) {
					packageName = this.project.getArtifactId();
				} else {
					StringTokenizer st = new StringTokenizer(gforgePackageName,
							"/");
					while (st.hasMoreTokens())
						packageName = st.nextToken();
				}
				String folder = project.getBasedir().toString() + "/"
						+ uploadFilePath + gforgePackageName + "/target/";
				getLog().info("searching for files in folder: " + folder);
				this.getGForgeLib().uploadFilesToGForge(
						this.getGForgeSessionID(), fileTypes, filePrefix, folder,
						this.getReleaseNumber(packageName), packageName,
						gforgeProjectID, releaseNotes, releaseChanges,
						processorType);
				this.getGForgeLib().addLog("\nUPLOADING: \nrelease number: "+this.getReleaseNumber(packageName)+
						"\npackage name: "+packageName+
						"\nproject ID: "+gforgeProjectID);
			}
			getLog().info("\nLog-messages: \n"+this.getGForgeLib().getLog());
			if (!this.getGForgeLib().getErrorLog().trim().equals(""))
				getLog().warn("\nError log messages:"+this.getGForgeLib().getErrorLog());
		} else {
			getLog().warn("Not a parent project! Doing nothing");
		}
	}

	/**
	 * Gets the release number for a given artifact name
	 * 
	 * @param artifactName the artifact name
	 * 
	 * @return the release number. If a release number is set in the
	 *         configuartion file or by a runtime parameter this number is used
	 *         for the parent project and all subprojects!
	 */
	public String getReleaseNumber(String artifactName) {

		if (this.releaseNumber != null) {
			return this.releaseNumber;
		} else {
			File file = new File(project.getBasedir().toString()
					+ "/gforge.releasenumber");
			String version = null;
			if (file.exists())
				version = getGForgeConfigData(artifactName,file);
			else
				throw new RuntimeException("File "+file+" not found!");
			if (version == null)
				getLog().warn("release number is 'null' !");
			return version;
		}
	}

	/**
	 * Gets the GForge user data from a GForge ConfigDataFile.
	 * 
	 * @param key
	 *            the key, e.g. "user" or "password"
	 * 
	 * @return the value to the selected key
	 */
	public String getGForgeConfigData(String key, File file) {
		String value = "";
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			Document doc = builder.parse(file);
			// first get the root node 
			Node rootNode = doc.getFirstChild();
			// the child nodes contain the configuration data (user, password)
			NodeList nodes = rootNode.getChildNodes();
			for (int i = 0; i < nodes.getLength(); i++) {
				// check if node name equals key
				if (nodes.item(i).getNodeName().trim().equals(key.trim()))
					value = nodes.item(i).getTextContent();
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			System.out.println("GForge configfile malformed?");
			System.out.println("filename: "+file.toString());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("GForge configfile file not found");
			System.out.println("filename: "+file.toString());
			e.printStackTrace();
		}
		return value;
	}

	private GForgeLib getGForgeLib() {
		return gForgeLib;
	}

	private void setGForgeLib(GForgeLib gfl) {
		this.gForgeLib = gfl;
	}

	private String getGForgeSessionID() {
		return gForgeSessionID;
	}

	/**
	 * Sets the GForge session id.
	 * 
	 * @param gforgeSessionID
	 *            the new GForge session id
	 */
	private void setGForgeSessionID(String gforgeSessionID) {
		this.gForgeSessionID = gforgeSessionID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	protected void finalize() {
		//close session
		this.getGForgeLib().closeGForgeSession(this.getGForgeSessionID());
		try {
			super.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

}
